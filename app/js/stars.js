const stars = document.querySelectorAll('.reviews__star');

stars.forEach(star => star.addEventListener('mouseover', e => {
    const children = [...star.parentElement.children];
    let st = [],
        cl = [],
        flag = false;

    for(let i=0; i < children.length; i++){
        const chi = children[i]
        if(e.target === chi){
            st.push(chi);
            flag = true;
            continue;
        }
        if (!flag){
            st.push(chi)
        }else{
            cl.push(chi);
        }
    }

    st.forEach(item => {
        item.classList.remove('icon-star-empty');
        item.classList.add('icon-star-full');
    })

    cl.forEach(item => {
        item.classList.remove('icon-star-full');
        item.classList.add('icon-star-empty');
    })
}));
