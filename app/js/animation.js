(function(){
    const images = document.querySelectorAll('.hero__img, .features__img');
    const cb = (entries, observer) => {
        entries.forEach(item => {
            if (item.isIntersecting){
                item.target.classList.add('fadeIn');
            }
        });
    }
    const opt = {};
    const obs = new IntersectionObserver(cb, opt);
    images.forEach(img => {
        obs.observe(img);
    });
})();

(function(){
    const elements = document.querySelectorAll('.hero__desc');
    const opt = {};
    const observer = new IntersectionObserver(
        function(entries, observer){
            entries.forEach(item => {
                if (item.isIntersecting){
                    setTimeout(function(){
                        item.target.classList.add('fadeIn');
                    }, 1050);
                }
            });
        },
        opt);
    elements.forEach(function(elem) {
        observer.observe(elem);
    });
})();

(function(){
    const elements = document.querySelectorAll('h1, .hero__link-wrapper, .hero__btn');
    const opt = {};
    const observer = new IntersectionObserver(
        function(entries, observer){
            entries.forEach(item => {
                if (item.isIntersecting){
                    setTimeout(function(){
                        let classArr = Array.from(item.target.classList);
                        if(window.innerWidth < 768 && (classArr.includes('hero__btn') || classArr.includes('hero__link-wrapper'))){
                            item.target.style.animation = item.target.dataset.animation;
                            item.target.style.opacity = "1";
                        }else{
                            item.target.classList.add('fadeIn');
                        }

                    }, 750);
                }
            });
        },
        opt);

    elements.forEach(function(elem) {
        observer.observe(elem);
    });
})();


(function(){
    const cards = document.querySelectorAll('.product__card, .product__btn');
    const opt = {};
    let sleep = 500;
    const observer = new IntersectionObserver(
        function(entries, observer){
            entries.forEach(item => {
               if (item.isIntersecting){
                   setTimeout(function (){
                       let clsAttr = Array.from(item.target.classList);
                       if (clsAttr.includes('product__btn')){
                           item.target.style.animation = item.target.dataset.animation;
                           item.target.style.opacity = "1";
                       }
                       item.target.classList.add('fadeIn');
                   }, sleep);
                   if(window.innerWidth > 768){
                       sleep += 500;
                   }
               }
            });
        },
    opt);

    cards.forEach(function(elem) {
        observer.observe(elem);
    });
})();

(function(){
    const elems = document.querySelectorAll('.features__subtitle, .features__title, .features__text, .features__item');
    const opt = {};
    let sleep = 300;
    const observer = new IntersectionObserver(
        function(entries, observer){
            entries.forEach(item => {
                if(item.isIntersecting){
                   setTimeout(function(){
                       let clsAttr = Array.from(item.target.classList);
                       if(clsAttr.includes('features__subtitle--top')){sleep=300;}
                       item.target.classList.add('fadeIn');
                   }, sleep);
                    sleep += 150;
                }
            });
        },
    opt);

    elems.forEach(function(elem) {
        observer.observe(elem);
    });
})();

const typingEffect = (str, elem) => {
    const speed = 75;
    let tempText = '';
    let i = 0;
    if(i < str.length ){
        tempText += str[i]
        elem.innerText = tempText;
        i++;
        setTimeout(typingEffect, speed);
    }
}


(function(){
    const elems = document.querySelectorAll('.process__text-cloud');
    const opt = {};
    let sleep = 400;
    const speed = 70;
    const observer = new IntersectionObserver(
        function(entries, observer){
            entries.forEach(item => {
                if(item.isIntersecting){
                    const str = item.target.dataset.text.split('');
                    const element = item.target.querySelector('.process__person-text');
                    setTimeout(function(){
                        let i = 0;
                        let tempText = '';
                        function test(){
                            if(i < str.length ){
                                tempText += str[i]
                                element.innerText = tempText;
                                i++;
                                setTimeout(test, speed);
                            }
                        }
                        test();
                   }, sleep);
                    sleep += speed * str.length;
                }
                //observer.disconnect();
            })
        },
    opt);

      elems.forEach(function(elem) {
        observer.observe(elem);
    });
})();

