const btn = document.querySelector(".nav__switch-btn");
const html = document.querySelector("html");

btn.addEventListener("click", () => {
    document.documentElement.setAttribute("data-theme",
        document.documentElement.getAttribute("data-theme") === "dark" ? "light" : "dark");
    localStorage.setItem("theme", localStorage.getItem("theme") === "light" ? "dark" : "light");
});
