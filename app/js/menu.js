window.onload = () => {
    let burger = document.querySelector('.hamburger');
    let header_menu = document.querySelector('.nav__menu');
    let body = document.querySelector('body');
    burger.addEventListener('click',  ()=>{
        burger.classList.toggle('active');
        header_menu.classList.toggle('active');
        body.classList.toggle('lock');
    });


    let theme = localStorage.getItem('theme');
    if (theme === ''){
        localStorage.setItem('theme', 'light');
        document.documentElement.setAttribute('data-theme', 'light');
    }else if(theme === 'light'){
        document.documentElement.setAttribute('data-theme', 'light');
    }else if(theme === 'dark'){
        document.documentElement.setAttribute('data-theme', 'dark');
    }
};