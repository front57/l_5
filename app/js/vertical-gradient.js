const rows = document.querySelectorAll('.row');
const maxWidth = 1085;
const minWidth = 576;

window.addEventListener('load', function(){
    const grdLeft = document.querySelectorAll('.gradient-l');
    const grdRight = document.querySelectorAll('.gradient-r');
    Array.from(grdLeft).forEach(elem => elem.style.display = "none")
    if(window.innerWidth < minWidth || window.innerWidth > maxWidth){
        Array.from(grdRight).forEach(elem => elem.style.display = "none");
    }
});


rows.forEach(row => {
    row.addEventListener('scroll', listenRightGrd);
    window.addEventListener('resize', listenRightGrdRes);
    row.addEventListener('scroll', listenLeftGrd);
    window.addEventListener('resize', listenLeftGrdRes);
});

function listenRightGrdRes(){
    const grdRight = document.querySelectorAll('.gradient-r');
    if (window.innerWidth >= maxWidth || window.innerWidth < minWidth){
        grdRight.forEach(elem => elem.style.display = "none");
    }else{
        grdRight.forEach(elem => elem.style.display = "block");
    }
}

function listenLeftGrdRes(){

    const grdLeft = document.querySelectorAll('.gradient-l');
    if (window.innerWidth >= maxWidth || window.innerWidth < minWidth){
        grdLeft.forEach(elem => elem.style.display = "none");
    }else{
        grdLeft.forEach(elem => elem.style.display = "block");
    }
    checkOnStartPos();
}

function listenRightGrd(){
    const grdRight = this.querySelector('.gradient-r');
    grdRight.style.right = -this.scrollLeft + 'px';
    if(this.scrollWidth === this.clientWidth + Math.round(this.scrollLeft)){
        grdRight.style.display = "none";
    } else if(window.innerWidth < maxWidth && window.innerWidth > minWidth){
        grdRight.style.display = 'block';

    }
}

function listenLeftGrd(){
    const grdLeft = this.querySelector('.gradient-l');
    grdLeft.style.left = this.scrollLeft + 'px';
    if(Math.round(this.scrollLeft) === 0){
        grdLeft.style.display = "none";
    }else if(window.innerWidth < maxWidth && window.innerWidth > minWidth){
        grdLeft.style.display = "block";
    }
}


function checkOnStartPos(){
    const grdLeft = document.querySelectorAll('.gradient-l');
    grdLeft.forEach(function(elem,i) {
        if(Math.round(elem.scrollLeft) === 0){
            elem.style.display = "none";
        }else{
            elem.style.display = "block";
        }
    });
}